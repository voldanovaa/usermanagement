package com.example.usermanagement.repository;

import com.example.usermanagement.model.PersonFull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonFullByIdRepository extends JpaRepository<PersonFull, Long> {

    @Query(value = "select\n" +
            "\tp.person_id,\n" +
            "\tp.name,\n" +
            "\tp.email,\n" +
            "\tp.password,\n" +
            "\tp.salary,\n" +
            "\ta.city,\n" +
            "\ta.country,\n" +
            "\ta.street,\n" +
            "\ta.postcode,\n" +
            "\tg.group_id,\n" +
            "\tg.name group_name,\n" +
            "\tg.description group_description,\n" +
            "\tr.role_id,\n" +
            "\tr.role_type,\n" +
            "\tr.description role_description\n" +
            "from\n" +
            "\tperson p inner join person_to_group pg\n" +
            "\ton p.person_id = pg.person_id\n" +
            "\tinner join \"group\" g\n" +
            "\ton pg.group_id = g.group_id\n" +
            "\tinner join group_to_role gr\n" +
            "\ton g.group_id = gr.group_id\n" +
            "\tinner join \"role\" r\n" +
            "\ton gr.role_id = r.role_id\n" +
            "\tinner join address a on\n" +
            "\tp.address_id = a.address_id\n" +
            "where\n" +
            "\tp.person_id = :personId", nativeQuery = true)
     List<PersonFull> getPersonFullById(@Param("personId")Long personId);

}


