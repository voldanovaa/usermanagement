package com.example.usermanagement.repository;

import com.example.usermanagement.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonByEmailRepository extends JpaRepository<Person, Long> {

     @Query(value = "select person_id, address_id, name, email from person where email = :email", nativeQuery = true)
    List<Person> getPersonByEmail(@Param("email")String email);

}


