package com.example.usermanagement.repository;

import com.example.usermanagement.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RolesByPersonIdRepository extends JpaRepository<Role, Long> {

    @Query(value = "select\n" +
            "\tr.role_id,\n" +
            "\tr.role_type,\n" +
            "\tr.description\n" +
            "from\n" +
            "\tperson p inner join person_to_group pg\n" +
            "\ton p.person_id = pg.person_id\n" +
            "\tinner join \"group\" g\n" +
            "\ton pg.group_id = g.group_id\n" +
            "\tinner join group_to_role gr\n" +
            "\ton g.group_id = gr.group_id\n" +
            "\tinner join \"role\" r\n" +
            "\ton gr.role_id = r.role_id\n" +
            "where\n" +
            "\tp.person_id = :personId", nativeQuery = true)
     List<Role> getRolesByPersonId(@Param("personId")Long personId);

}


