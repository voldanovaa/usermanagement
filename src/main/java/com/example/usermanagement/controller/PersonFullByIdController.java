package com.example.usermanagement.controller;

import com.example.usermanagement.model.PersonFull;
import com.example.usermanagement.repository.PersonFullByIdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class PersonFullByIdController {

    @Autowired
    private PersonFullByIdRepository personfullbyidRepository;

    @GetMapping("/persons/full/{personId}")
    public List<PersonFull> getPersonFullById(@PathVariable Long personId) {
        return personfullbyidRepository.getPersonFullById(personId);
    }

}


















