package com.example.usermanagement.controller;

import com.example.usermanagement.model.Role;
import com.example.usermanagement.repository.RolesByPersonIdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class RolesByPersonIdController {

    @Autowired
    private RolesByPersonIdRepository rolesbypersonidRepository;

    @GetMapping("/roles/{personId}")
    public List<Role> getRolesByPersonId(@PathVariable Long personId) {
        return rolesbypersonidRepository.getRolesByPersonId(personId);
    }

}


















