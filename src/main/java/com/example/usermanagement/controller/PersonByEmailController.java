package com.example.usermanagement.controller;

import com.example.usermanagement.model.Person;
import com.example.usermanagement.repository.PersonByEmailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class PersonByEmailController {

    @Autowired
    private PersonByEmailRepository personbyemailRepository;

    @GetMapping("/persons/byEmail/{email}")
    public List<Person> getPersonByEmail(@PathVariable String email) {
        return personbyemailRepository.getPersonByEmail(email);
    }

}


















