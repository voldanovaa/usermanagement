package com.example.usermanagement.controller;

import com.example.usermanagement.model.Person;
import com.example.usermanagement.repository.PersonByIdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class PersonByIdController {

    @Autowired
    private PersonByIdRepository personbyidRepository;

    @GetMapping("/persons/{personId}")
    public List<Person> getPersonById(@PathVariable Long personId) {
        return personbyidRepository.getPersonById(personId);
    }

}


















