package com.example.usermanagement.model;

import javax.persistence.*;

@Entity
public class Role {
    @Id
    @GeneratedValue
    private Long role_id;
    private String role_type;
    private String description;

    public Long getRoleId() { return role_id; }
    public void setRoleId(Long role_id) { this.role_id = role_id; }

    public String getRoleType() { return role_type; }
    public void getRoleType(String role_type) { this.role_type = role_type; }

    public String getDescription() { return description; }
    public void getDescription(String description) { this.description = description; }

}