package com.example.usermanagement.model;

import javax.persistence.*;

@Entity
public class PersonFull {
    @Id
    @GeneratedValue
    private Long person_id;
    private String name;
    private String email;
    private String password;
    private String salary;
    private String city;
    private String country;
    private String street;
    private String postcode;
    private String group_id;
    private String group_name;
    private String group_description;
    private String role_id;
    private String role_type;
    private String role_description;

    public Long getPersonId() { return person_id; }
    public void setPersonId(Long person_id) { this.person_id = person_id; }

    public String getName() { return name; }
    public void getName(String name) { this.name = name; }

    public String getEmail() { return email; }
    public void getEmail(String email) { this.email = email; }

    public String getPassword() { return password; }
    public void getPassword(String password) { this.password = password; }

    public String getSalary() { return salary; }
    public void getSalary(String salary) { this.salary = salary; }

    public String getCity() { return city; }
    public void getCity(String city) { this.city = city; }

    public String getCountry() { return country; }
    public void getCountry(String country) { this.country = country; }

    public String getStreet() { return street; }
    public void getStreet(String street) { this.street = street; }

    public String getPostcode() { return postcode; }
    public void getPostcode(String postcode) { this.postcode = postcode; }

    public String getGroupId() { return group_id; }
    public void getGroupId(String group_id) { this.group_id = group_id; }

    public String getGroupName() { return group_name; }
    public void getGroupName(String group_name) { this.group_name = group_name; }

    public String getDescription() { return group_description; }
    public void getDescription(String group_description) { this.group_description = group_description; }

    public String getRoleId() { return role_id; }
    public void getRoleId(String role_id) { this.role_id = role_id; }

    public String getRoleType() { return role_type; }
    public void getRoleType(String role_type) { this.role_type = role_type; }

    public String getRoleDescription() { return role_description; }
    public void getRoleDescription(String role_description) { this.role_description = role_description; }

}