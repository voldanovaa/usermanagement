package com.example.usermanagement.model;

import javax.persistence.*;

@Entity
public class Person {
    @Id
    @GeneratedValue
    private Long person_id;
    private Long address_id;
    private String name;
    private String email;

    public Long getPersonId() { return person_id; }
    public void setPersonId(Long person_id) { this.person_id = person_id; }

    public Long getAddressId() { return address_id; }
    public void getAddressId(Long address_id) { this.address_id = address_id; }

    public String getName() { return name; }
    public void getName(String name) { this.name = name; }

    public String getEmail() { return email; }
    public void getEmail(String email) { this.email = email; }

}