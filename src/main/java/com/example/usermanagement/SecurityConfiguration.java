package com.example.usermanagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter
{
	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		http
				.authorizeRequests()
				/*
				Vsechny informace o osobe (DOTAZ 4) muze pouze uzivatel s roli MANAGER
				 */
				.antMatchers("/persons/full/**").hasRole("MANAGER")
				/*
				REST resource neni mozne volat, pokud neni uzivatel autentizovan
				 */
				.antMatchers("/**").fullyAuthenticated()

				.and()
				.httpBasic();
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth)
			throws Exception
	{
		auth.inMemoryAuthentication()
				.withUser("admin")
				.password("{noop}admin")
				.roles("USER");

		auth.inMemoryAuthentication()
				.withUser("manager")
				.password("{noop}manager")
				.roles("MANAGER");
	}
}